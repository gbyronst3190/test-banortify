$(".icon-play").on("click",function(){
  var selector = $(this);
  $('.tabla_style>tbody>tr').each(function(){
    if ($(this).find(".seleccionado")) {
      $(this).removeClass("seleccionado");
      selector.css("display","");
      selector.parents().find(".icon-pause, .icon-stop").css("display","none");
      $(".icon-play").css("display","");
    }
    selector.parents("tr").addClass("seleccionado");
    selector.parent().find(".icon-pause, .icon-stop").css("display","");
    selector.css("display","none");
  });
});

$(".icon-pause , .icon-stop").on("click",function(){
  var selector = $(this);
  selector.parent().find(".icon-play").css("display","");
  selector.parent().find(".icon-stop").css("display","none");
  selector.css("display","none");
  selector.parents("tr").removeClass("seleccionado");
});


$(".icon-stop").on("click",function(){
  var selector = $(this);
  selector.parent().find(".icon-play").css("display","");
  selector.parent().find(".icon-pause").css("display","none");
  selector.css("display","none");
});
$(".icon-cancel").on("click", function(){
  var selector = $(this);
  selector.parents("tr").remove();
});

$(".icon-up-big").on("click",function(){
  var row = $(this).parents("tr:first");
  row.insertBefore(row.prev());
});
$(".icon-down-big").on("click",function(){
  var row = $(this).parents("tr:first");
  row.insertAfter(row.next());
});
