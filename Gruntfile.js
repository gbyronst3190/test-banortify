module.exports = function(grunt){
  grunt.initConfig({
    uglify:{
      todos_js:{
        files:{
          'dist/ejemplo.min.js':['app/js/controllers/*.js','app/js/models/*.js']
        }
      }
    },

    clean : {
      dist: {
        files: [{
          dot: true,
          src: [
            'dist/*'
          ]
        }]
      },
      limpia_watch:{
        files: [{
          dot: true,
          src: [
            'dist/*.css','dist/*.js'
          ]
        }]
      }
    },
    sass: {
      todos_sass: {
        files: {
          "dist/main.css": "app/sass/*.css"
        }
      }
    },
    autoprefixer : {
      options:{
        browser : ['last 2 version','ie 9']
      },
      single_file:{
        src:'dist/main.css.css'
      }
    },
    compass: {
      dist: {
        options: {
          config: 'config.rb',
          sourcemap: true
        }

      }//dist
    },//compass
    copy: {
      copia_a_dis: {
        files: [
          {expand: true, cwd: 'app/sass', src: ['**/*.css'], dest: 'dist/copy'}
        ]
      },
      copia_a_sass: {
        files: [
          {expand: true, cwd: 'node_modules/bootstrap/dist', src: ['**/*'], dest: 'app/sass/resources'}
        ]
      },
    },
    connect:{
      persistente: {
        options: {
          port: 9002,
          open: false,
          base: 'dist/',
          keepalive: true
        }
      }
    },
    watch: {
      files: {
        files: [ 'app/js/controllers/*.js','app/js/models/*.js','app/sass/*.sass','app/sass/*.scss'],
        tasks :['escucha'],
        options: {
          reload: true
        }
      },
      scss: {
         files: ["app/sass/*.scss"],
         tasks: ['compass','autoprefixer']
       } //scss
    }
  });

  //cargamos la tarea
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-compass');
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-connect');

  //ejecutamos tareas
  grunt.registerTask('ui', function (target) {
    grunt.log.ok("realizando tareas .......");
    grunt.task.run([
      'clean:dist',
      'uglify:todos_js',
      'sass:todos_sass',
      'copy:copia_a_dis',
      'copy:copia_a_sass',
      'watch:files',
      'connect:persistente',
    ]);
  });

  grunt.registerTask('escucha',function(target){
    grunt.log.ok("actualizando por watch..........");
    grunt.task.run([
      'clean:limpia_watch',
      'uglify:todos_js',
      'sass:todos_sass',
      'watch:files',
    ])
  });

};
